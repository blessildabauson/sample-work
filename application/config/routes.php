<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 			= 'user';
$route['404_override'] 					= '';
$route['translate_uri_dashes'] 			= FALSE;

$route['dashboard'] 					= 'dashboard';
$route['login'] 						= 'user';

$route['user/login'] 					= 'user/login';
$route['user/logout'] 					= 'user/logout';
$route['user/staff'] 					= 'user/staff';
$route['user/add_staff'] 				= 'user/add_staff';
$route['user/edit_staff/(:num)'] 		= 'user/edit_staff/$1';
$route['user/delete_staff/(:num)'] 		= 'user/delete_staff/$1';

$route['clinic'] 						= 'clinic';

$route['patient/lists'] 				= 'patient/lists';
$route['patient/add'] 					= 'patient/add';
$route['patient/edit/(:num)'] 			= 'patient/edit/$1';
$route['patient/delete/(:num)'] 		= 'patient/delete/$1';
$route['patient/record/(:num)'] 		= 'patient/record/$1';
$route['patient/add_certificate/(:num)']= 'patient/add_certificate/$1';
$route['patient/use_certificate/(:num)']= 'patient/use_certificate/$1';

$route['consultation'] 					= 'consultation';
$route['consultation/add'] 				= 'consultation/add';
$route['consultation/delete/(:num)'] 	= 'consultation/delete/$1';

$route['inventory'] 					= 'inventory';
$route['inventory/add'] 				= 'inventory/add';
$route['inventory/edit/(:num)'] 		= 'inventory/edit/$1';
$route['inventory/delete/(:num)'] 		= 'inventory/delete/$1';
$route['inventory/vaccine_kind'] 		= 'inventory/vaccine_kind';

$route['schedule'] 						= 'schedule';
$route['schedule/add'] 					= 'schedule/add';
$route['schedule/edit/(:num)'] 			= 'schedule/edit/$1';
$route['schedule/delete/(:num)'] 		= 'schedule/delete/$1';

$route['queue'] 				 		= 'queue';
$route['queue/add'] 				 	= 'queue/add';
$route['queue/qlist'] 				 	= 'queue/qlist';
$route['queue/status/(:num)/(:any)'] 	= 'queue/status/$1/$2';

$route['certificate/templates'] 	 			= 'certificate/templates';
$route['certificate/templates_add']  			= 'certificate/templates_add';
$route['certificate/templates_edit/(:num)'] 	= 'certificate/templates_edit/$1';
$route['certificate/templates_delete/(:num)'] 	= 'certificate/templates_delete/$1';
$route['certificate/patients'] 	 				= 'certificate/patients';
$route['certificate/patients_delete/(:num)'] 	= 'certificate/patients_delete/$1';