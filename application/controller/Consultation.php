<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultation extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
	     parent::__construct();
	     $this->load->model('UserModel');
	     $this->load->model('PatientModel');
	     $this->load->model('PatientConsultationModel');
	     $this->load->model('PatientVaccinationModel');
	     $this->load->model('VaccinationKindModel');
	     $this->load->model('ConsultationModel');
	     $this->load->model('PatientMedicationModel');
           $this->load->model('InventoryModel');
	}

	public function dump($arr){
	    echo "<pre>";
	    print_r($arr);
	    echo "</pre>";
	}

	public function index(){
            if($this->session->userdata('logged_in')) {
                  $data['user'] = $this->session->userdata('logged_in');
                  $data['post'] = array();
                  if($_SERVER['REQUEST_METHOD'] == "POST"){
                        $data['consultations']  = $this->ConsultationModel->getAllConsultations($_POST);
                        $data['post']           = $_POST;
                  }else{
                        $data['consultations'] = $this->ConsultationModel->getAllConsultations();
                  }
		      $this->load->view('consultation.php', $data);
            } else {
                  redirect('login', 'refresh');
            }
	}

	public function add($patient_id = 0){
		if($this->session->userdata('logged_in')) {
                  $data['user'] 	 	= $this->session->userdata('logged_in');
                  $data['patient_id'] 	= $patient_id;
                  $data["vaccine_kinds"] 	= $this->VaccinationKindModel->getAll();
                  $data["patient"] 		= $this->PatientModel->getOneWhere("patient_id", $patient_id);
                  $data['inv_vacs']       = $this->InventoryModel->getWhere('inventory_type_id', 2);
                  $data['inv_meds']       = $this->InventoryModel->getWhere('inventory_type_id !=', 2);

                  if($_SERVER['REQUEST_METHOD'] == "POST"){
                  	$consultation =  $this->ConsultationModel->create([
      					'type_visit'      	=> $_POST['type_visit'],
      					'date_visit'          	=> $_POST['date_visit'],
      					'weight'         		=> $_POST['weight'],
      					'heights'     		=> $_POST['heights'],
      					'diagnosis'         	=> $_POST['diagnosis'],
      					'private_notes'       	=> $_POST['private_notes'],
      					'other_instruction'   	=> $_POST['other_instruction'],
      					'next_visit'        	=> $_POST['next_visit'],
      					'reason_next_visit'     => $_POST['reason_next_visit']
      				]);

                        $patient_consultation = $this->PatientConsultationModel->create([
                                    'patient_id'            => $_POST['patient_id'],
                                    'consultation_id'       => $consultation->consultation_id,
                                    'date_added'            => date("Y-m-d H:i:s")
                              ]);

                  	$vaccines = array();
                  	if(isset($_POST['vaccine']) && count($_POST['vaccine']) > 0){
                  		foreach($_POST['vaccine'] as $vkey => $vval){
                  			$vaccines[] = array(
                  				"patient_id" 	  	  	=> $_POST['patient_id'],
                  				"consultation_id" 		=> $consultation->consultation_id,
                  				"vaccination_kind_id" 	      => $_POST['vaccine'][$vkey],
                  				"dose" 				=> $_POST['dose'][$vkey],
                  				"date_added" 			=> $_POST['date_given'][$vkey],
                  				"batch_no" 				=> $_POST['batch_no'][$vkey],
                  				"remarks" 				=> $_POST['remarks'][$vkey],
                  				"next_sched" 			=> $_POST['next_sched'][$vkey],
                                          "inventory_id"                => $_POST['v_inventory_id'][$vkey]
                  			);
                  		}
                  		foreach($vaccines as $vaccVal){
                  			$this->PatientVaccinationModel->create($vaccVal);
                  		}
                  	}

                  	$meds = array();
                  	if(isset($_POST['generic_name']) && count($_POST['generic_name']) > 0){
                  		foreach($_POST['generic_name'] as $mkey => $mval){
                  			$meds[] = array(
                  				"patient_id" 	  	=> $_POST['patient_id'],
                  				"consultation_id" 	=> $consultation->consultation_id,
                  				"inventory_id" 		=> $_POST['inventory_id'][$mkey],
                  				"generic_name_med" 	=> $_POST['generic_name'][$mkey],
                  				"quantity_med" 		=> $_POST['quantity'][$mkey],
                  				"date_added" 		=> date("Y-m-d H:i:s"),
                  				"med_dose" 			=> $_POST['med_dose'][$mkey],
                  				"dose_unit" 		=> $_POST['dose_unit'][$mkey],
                  				"stock_dose" 		=> $_POST['stock_dose'][$mkey],
                  				"duration" 			=> $_POST['duration'][$mkey],
                  				"duration_unit" 	      => $_POST['duration_unit'][$mkey],
                  				"instruction" 		=> $_POST['instruction'][$mkey],
                  				"frequency" 		=> $_POST['frequency'][$mkey],
                                          "inventory_id"          => $_POST['m_inventory_id'][$mkey]
                  			);
                  		}
                  		foreach($meds as $medVal){
                  			$this->PatientMedicationModel->create($medVal);
                  		}
                  	}

                        redirect('patient/record/'.$_POST['patient_id'], 'refresh');
                  }
                  $this->load->view('consultation_add.php', $data);
            } else {
                  redirect('login', 'refresh');
            }
	}

      public function delete($id = 0){
        if($this->session->userdata('logged_in') && !empty($id)) {
            $this->ConsultationModel->delete($id);
            $this->session->set_flashdata('success', 'Successfully deleted consultation.');
            redirect('/consultation', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

}
