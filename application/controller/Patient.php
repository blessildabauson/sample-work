<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
	    parent::__construct();
	    $this->load->model('UserModel');
	    $this->load->model('PatientModel');
	    $this->load->model('ClinicModel');
	    $this->load->model('PatientQueueModel');
	    $this->load->model('PatientConsultationModel');
	    $this->load->model('PatientMedicationModel');
	    $this->load->model('PatientVaccinationModel');
		$this->load->model('ScheduleModel');
		$this->load->model('VaccinationKindModel');
	    $this->load->model('MedCertTemplateModel');
	    $this->load->model('PatientMedCertModel');

	    $this->load->library('pagination');
	}

	public function dump($arr){
	    echo "<pre>";
	    print_r($arr);
	    echo "</pre>";
	}

	public function lists(){
		if($this->session->userdata('logged_in')) {
            $data['user'] 	  = $this->session->userdata('logged_in');
            $data["clinics"]  = $this->ClinicModel->getAll();

            $config = array();
			$config["base_url"] 		= base_url(). "patient/lists";
			$total_row 					= $this->PatientModel->record_count($_POST);
			$config["total_rows"] 	    = $total_row;
			$config["per_page"] 		= 25;
			$config['num_links'] 		= 2;
			$config['cur_tag_open'] 	= '&nbsp;<a class="current">';
			$config['cur_tag_close'] 	= '</a>';
			$config['next_link'] 		= 'Next';
			$config['prev_link'] 		= 'Previous';
    		//$config['page_query_string']= FALSE;

			$this->pagination->initialize($config);
			if($this->uri->segment(3)){
				$page = ($this->uri->segment(3)) ;
			}else{
				$page = 0;
			}

            $data['post'] = array();
            if($_SERVER['REQUEST_METHOD'] == "POST"){
       			$data['patients'] = $this->PatientModel->getPatientsList($config["per_page"], $page, $_POST);
       			$data['post'] 	  = $_POST;
       		}else{
       			$data['patients'] = $this->PatientModel->getPatientsList($config["per_page"], $page);
       		}

       		$str_links = $this->pagination->create_links();
       		$data["links"] = explode('&nbsp;',$str_links);
            $this->load->view('patient.php', $data);
        } else {
            redirect('login', 'refresh');
        }
	}

	public function add(){
		if($this->session->userdata('logged_in')) {
			$data['user'] 	  = $this->session->userdata('logged_in');
            $data["clinics"]  = $this->ClinicModel->getAll();
            if($_SERVER['REQUEST_METHOD'] == "POST"){
       			$_POST['date_added']  = date("Y-m-d H:i:s");
       			$_POST['last_update'] = date("Y-m-d H:i:s");
       			$patient = $this->PatientModel->create($_POST);
       			$this->session->set_flashdata('success', 'Successfully added patient.');
            	redirect('/patient/lists', 'refresh'); 
       		}
            $this->load->view('patient_add.php', $data);
        } else {
            redirect('login', 'refresh');
        }
	}

	public function edit($id = 0){
		if($this->session->userdata('logged_in')) {
			$data['user'] 	  = $this->session->userdata('logged_in');
            $data["clinics"]  = $this->ClinicModel->getAll();
            $data["patient"]  = array();
            if(!empty($id)){
            	$data["patient"] = $this->PatientModel->getOneWhere("patient_id", $id);
            }
            if($_SERVER['REQUEST_METHOD'] == "POST"){
            	$_POST['last_update'] = date("Y-m-d H:i:s");
       			$this->PatientModel->updateDB($_POST);
       			$this->session->set_flashdata('success', 'Successfully updated patient.');
            	redirect('/patient/lists', 'refresh'); 
       		}
            $this->load->view('patient_edit.php', $data);
        } else {
            redirect('login', 'refresh');
        }
	}

	public function delete($id = 0){
		if($this->session->userdata('logged_in') && !empty($id)) {
			$this->PatientModel->delete($id);
            $this->session->set_flashdata('success', 'Successfully deleted patient.');
            redirect('/patient/lists', 'refresh'); 
        }
	}

	public function record($patient_id = 0){
		if($this->session->userdata('logged_in') && !empty($patient_id)) {
			$data['user'] 	  		= $this->session->userdata('logged_in');
			$data['patient_id'] 	= $patient_id;
            $data["patient"] 		= $this->PatientModel->getOneWhere("patient_id", $patient_id);
            $patientVacs 			= $this->PatientVaccinationModel->getAllPatientVaccinations($patient_id);
            $vaccineKinds			= $this->VaccinationKindModel->getAll();
            $data['consultations'] 	= $this->PatientConsultationModel->getPatientConsulations($patient_id);
            $data['medications'] 	= $this->PatientMedicationModel->getPatientMedications($patient_id);
            $data['certs'] 			= $this->PatientMedCertModel->getWhere("patient_id", $patient_id);

            $vaccineType = array();
            if(count($vaccineKinds) > 0){
				foreach($vaccineKinds as $row){
					$vaccineType[$row->label] = $row;
					$doseCnt = ($row->label == "Influenza" || $row->label == "DTwP/DTaP") ? 10: $row->total_dose;
					for($i = 1; $i <= $doseCnt; $i++){
						if(array_key_exists($row->label, $patientVacs)){
							if(array_key_exists($i, $patientVacs[$row->label])){
								$vaccineType[$row->label]->patient[$i] = $patientVacs[$row->label][$i];
							}else{
								$vaccineType[$row->label]->patient[$i] = array(
									"date" 	  => "",
									"details" => "",
									"remarks" => ""
								);
							}
						}else{
							$vaccineType[$row->label]->patient[$i] = array(
								"date" 	  => "",
								"details" => "",
								"remarks" => ""
							);
						}
					}
				}
			}
			
            $data["vaccines"] = $this->buildVaccines($vaccineType);
			$this->load->view('patient_record.php', $data);
        } else {
            redirect('login', 'refresh');
        }
	}

	public function add_certificate($patient_id = 0){
		if($this->session->userdata('logged_in')) {
			$data['user'] 		= $this->session->userdata('logged_in');
			$data['patient_id'] = $patient_id;
            $data["patient"] 	= $this->PatientModel->getOneWhere("patient_id", $patient_id);
            $data['templates']	= $this->MedCertTemplateModel->getAll();
           
            if($_SERVER['REQUEST_METHOD'] == "POST"){
            	$cert = $this->PatientMedCertModel->create([
	                'patient_id' 			=> $_POST['patient_id'],
	                'med_cert_template_id'  => $_POST['med_cert_template_id'],
	                'purpose'    			=> $_POST['purpose'],
	                'content'    			=> $_POST['content'],
	                'date_added' 			=> date("Y-m-d H:i:s")
	            ]);
       			$this->session->set_flashdata('success', 'Successfully added certificate to patient.');
            	redirect('/patient/record/'.$patient_id, 'refresh'); 
       		}
            $this->load->view('patient_add_certificate.php', $data);
        } else {
            redirect('login', 'refresh');
        }
	}

	public function use_certificate($temp_id = 0){
		if($this->session->userdata('logged_in')) {
            if($_SERVER['REQUEST_METHOD'] == "POST"){
            	$template = $this->MedCertTemplateModel->getOneWhere("med_cert_template_id", $temp_id);
            	echo json_encode($template);
       		}
        } else {
            redirect('login', 'refresh');
        }
	}

	public function buildVaccines($vaccineType = array()){
		$vaccines = array();
		$vaccines[0] = array(
			"child_age" 	=> "At Birth",
			"vaccine_dose"  => "BCG (Dose 1 of ".$vaccineType['BCG']->total_dose.")",
			"protects" 		=> $vaccineType['BCG']->protect_against,
			"date" 			=> $vaccineType['BCG']->patient[1]['date'],
			"details" 		=> $vaccineType['BCG']->patient[1]['details'],
			"remarks"		=> $vaccineType['BCG']->patient[1]['remarks']
		);

		$vaccines[1] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Hepa B (Dose 1 of ".$vaccineType['Hepa B']->total_dose.")",
			"protects" 		=> $vaccineType['Hepa B']->protect_against,
			"date" 			=> $vaccineType['Hepa B']->patient[1]['date'],
			"details" 		=> $vaccineType['Hepa B']->patient[1]['details'],
			"remarks"		=> $vaccineType['Hepa B']->patient[1]['remarks']
		);

		$vaccines[2] = array(
			"child_age" 	=> "4 weeks to 8 weeks",
			"vaccine_dose"  => "Hepa B (Dose 2 of ".$vaccineType['Hepa B']->total_dose.")",
			"protects" 		=> $vaccineType['Hepa B']->protect_against,
			"date" 			=> $vaccineType['Hepa B']->patient[2]['date'],
			"details" 		=> $vaccineType['Hepa B']->patient[2]['details'],
			"remarks"		=> $vaccineType['Hepa B']->patient[2]['remarks']
		);

		$vaccines[3] = array(
			"child_age" 	=> "4 weeks to 8 weeks",
			"vaccine_dose"  => "Hepa B (Dose 2 of ".$vaccineType['Hepa B']->total_dose.")",
			"protects" 		=> $vaccineType['Hepa B']->protect_against,
			"date" 			=> $vaccineType['Hepa B']->patient[2]['date'],
			"details" 		=> $vaccineType['Hepa B']->patient[2]['details'],
			"remarks"		=> $vaccineType['Hepa B']->patient[2]['remarks']
		);

		$vaccines[3] = array(
			"child_age" 	=> "6 weeks to 8 weeks",
			"vaccine_dose"  => "DTaP (Dose 1 of ".$vaccineType['DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTaP']->protect_against,
			"date" 			=> $vaccineType['DTaP']->patient[1]['date'],
			"details" 		=> $vaccineType['DTaP']->patient[1]['details'],
			"remarks"		=> $vaccineType['DTaP']->patient[1]['remarks']
		);

		$vaccines[4] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Hib (Dose 1 of ".$vaccineType['Hib']->total_dose.")",
			"protects" 		=> $vaccineType['Hib']->protect_against,
			"date" 			=> $vaccineType['Hib']->patient[1]['date'],
			"details" 		=> $vaccineType['Hib']->patient[1]['details'],
			"remarks"		=> $vaccineType['Hib']->patient[1]['remarks']
		);

		$vaccines[5] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "OPV/IPV (Dose 1 of ".$vaccineType['OPV/IPV']->total_dose.")",
			"protects" 		=> $vaccineType['OPV/IPV']->protect_against,
			"date" 			=> $vaccineType['OPV/IPV']->patient[1]['date'],
			"details" 		=> $vaccineType['OPV/IPV']->patient[1]['details'],
			"remarks"		=> $vaccineType['OPV/IPV']->patient[1]['remarks']
		);

		$vaccines[6] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "PCV (Dose 1 of ".$vaccineType['PCV']->total_dose.")",
			"protects" 		=> $vaccineType['PCV']->protect_against,
			"date" 			=> $vaccineType['PCV']->patient[1]['date'],
			"details" 		=> $vaccineType['PCV']->patient[1]['details'],
			"remarks"		=> $vaccineType['PCV']->patient[1]['remarks']
		);

		$vaccines[7] = array(
			"child_age" 	=> "6 weeks to 8 months",
			"vaccine_dose"  => "Rotavirus (Dose 1 of ".$vaccineType['Rotavirus']->total_dose.")",
			"protects" 		=> $vaccineType['Rotavirus']->protect_against,
			"date" 			=> $vaccineType['Rotavirus']->patient[1]['date'],
			"details" 		=> $vaccineType['Rotavirus']->patient[1]['details'],
			"remarks"		=> $vaccineType['Rotavirus']->patient[1]['remarks']
		);

		$vaccines[8] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Rotavirus (Dose 2 of ".$vaccineType['Rotavirus']->total_dose.")",
			"protects" 		=> $vaccineType['Rotavirus']->protect_against,
			"date" 			=> $vaccineType['Rotavirus']->patient[2]['date'],
			"details" 		=> $vaccineType['Rotavirus']->patient[2]['details'],
			"remarks"		=> $vaccineType['Rotavirus']->patient[2]['remarks']
		);

		$vaccines[9] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Rotavirus (Dose 3 of ".$vaccineType['Rotavirus']->total_dose.")",
			"protects" 		=> $vaccineType['Rotavirus']->protect_against,
			"date" 			=> $vaccineType['Rotavirus']->patient[3]['date'],
			"details" 		=> $vaccineType['Rotavirus']->patient[3]['details'],
			"remarks"		=> $vaccineType['Rotavirus']->patient[3]['remarks']
		);

		$vaccines[10] = array(
			"child_age" 	=> "10 weeks to 16 weeks",
			"vaccine_dose"  => "Hepa B (Dose 3 of ".$vaccineType['Hepa B']->total_dose.")",
			"protects" 		=> $vaccineType['Hepa B']->protect_against,
			"date" 			=> $vaccineType['Hepa B']->patient[3]['date'],
			"details" 		=> $vaccineType['Hepa B']->patient[3]['details'],
			"remarks"		=> $vaccineType['Hepa B']->patient[3]['remarks']
		);

		$vaccines[11] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "DTaP (Dose 2 of ".$vaccineType['DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTaP']->protect_against,
			"date" 			=> $vaccineType['DTaP']->patient[2]['date'],
			"details" 		=> $vaccineType['DTaP']->patient[2]['details'],
			"remarks"		=> $vaccineType['DTaP']->patient[2]['remarks']
		);

		$vaccines[12] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Hib (Dose 2 of ".$vaccineType['Hib']->total_dose.")",
			"protects" 		=> $vaccineType['Hib']->protect_against,
			"date" 			=> $vaccineType['Hib']->patient[2]['date'],
			"details" 		=> $vaccineType['Hib']->patient[2]['details'],
			"remarks"		=> $vaccineType['Hib']->patient[2]['remarks']
		);

		$vaccines[13] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "OPV/IPV (Dose 2 of ".$vaccineType['OPV/IPV']->total_dose.")",
			"protects" 		=> $vaccineType['OPV/IPV']->protect_against,
			"date" 			=> $vaccineType['OPV/IPV']->patient[2]['date'],
			"details" 		=> $vaccineType['OPV/IPV']->patient[2]['details'],
			"remarks"		=> $vaccineType['OPV/IPV']->patient[2]['remarks']
		);

		$vaccines[14] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "PCV (Dose 2 of ".$vaccineType['PCV']->total_dose.")",
			"protects" 		=> $vaccineType['PCV']->protect_against,
			"date" 			=> $vaccineType['PCV']->patient[2]['date'],
			"details" 		=> $vaccineType['PCV']->patient[2]['details'],
			"remarks"		=> $vaccineType['PCV']->patient[2]['remarks']
		);

		$vaccines[15] = array(
			"child_age" 	=> "14 weeks to 6 months",
			"vaccine_dose"  => "DTaP (Dose 3 of ".$vaccineType['DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTaP']->protect_against,
			"date" 			=> $vaccineType['DTaP']->patient[3]['date'],
			"details" 		=> $vaccineType['DTaP']->patient[3]['details'],
			"remarks"		=> $vaccineType['DTaP']->patient[3]['remarks']
		);

		$vaccines[16] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Hib (Dose 3 of ".$vaccineType['Hib']->total_dose.")",
			"protects" 		=> $vaccineType['Hib']->protect_against,
			"date" 			=> $vaccineType['Hib']->patient[3]['date'],
			"details" 		=> $vaccineType['Hib']->patient[3]['details'],
			"remarks"		=> $vaccineType['Hib']->patient[3]['remarks']
		);

		$vaccines[17] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "OPV/IPV (Dose 3 of ".$vaccineType['OPV/IPV']->total_dose.")",
			"protects" 		=> $vaccineType['OPV/IPV']->protect_against,
			"date" 			=> $vaccineType['OPV/IPV']->patient[3]['date'],
			"details" 		=> $vaccineType['OPV/IPV']->patient[3]['details'],
			"remarks"		=> $vaccineType['OPV/IPV']->patient[3]['remarks']
		);

		$vaccines[18] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "PCV (Dose 3 of ".$vaccineType['PCV']->total_dose.")",
			"protects" 		=> $vaccineType['PCV']->protect_against,
			"date" 			=> $vaccineType['PCV']->patient[3]['date'],
			"details" 		=> $vaccineType['PCV']->patient[3]['details'],
			"remarks"		=> $vaccineType['PCV']->patient[3]['remarks']
		);

		$vaccines[19] = array(
			"child_age" 	=> "14 weeks to 12 months",
			"vaccine_dose"  => "Hepa B (Dose 4 of ".$vaccineType['Hepa B']->total_dose.")",
			"protects" 		=> $vaccineType['Hepa B']->protect_against,
			"date" 			=> $vaccineType['Hepa B']->patient[4]['date'],
			"details" 		=> $vaccineType['Hepa B']->patient[4]['details'],
			"remarks"		=> $vaccineType['Hepa B']->patient[4]['remarks']
		);

		$vaccines[20] = array(
			"child_age" 	=> "6 months and older",
			"vaccine_dose"  => "Influenza (Dose 1 of ".$vaccineType['Influenza']->total_dose.")",
			"protects" 		=> $vaccineType['Influenza']->protect_against,
			"date" 			=> $vaccineType['Influenza']->patient[1]['date'],
			"details" 		=> $vaccineType['Influenza']->patient[1]['details'],
			"remarks"		=> $vaccineType['Influenza']->patient[1]['remarks']
		);

		$vaccines[21] = array(
			"child_age" 	=> "9 months to 12 months",
			"vaccine_dose"  => "Measles (Dose 1 of ".$vaccineType['Measles']->total_dose.")",
			"protects" 		=> $vaccineType['Measles']->protect_against,
			"date" 			=> $vaccineType['Measles']->patient[1]['date'],
			"details" 		=> $vaccineType['Measles']->patient[1]['details'],
			"remarks"		=> $vaccineType['Measles']->patient[1]['remarks']
		);

		$vaccines[22] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "PCV (Dose 4 of ".$vaccineType['PCV']->total_dose.")",
			"protects" 		=> $vaccineType['PCV']->protect_against,
			"date" 			=> $vaccineType['PCV']->patient[4]['date'],
			"details" 		=> $vaccineType['PCV']->patient[4]['details'],
			"remarks"		=> $vaccineType['PCV']->patient[4]['remarks']
		);

		$vaccines[23] = array(
			"child_age" 	=> "12 months to 15 months",
			"vaccine_dose"  => "Varicella (Dose 1 of ".$vaccineType['Varicella']->total_dose.")",
			"protects" 		=> $vaccineType['Varicella']->protect_against,
			"date" 			=> $vaccineType['Varicella']->patient[1]['date'],
			"details" 		=> $vaccineType['Varicella']->patient[1]['details'],
			"remarks"		=> $vaccineType['Varicella']->patient[1]['remarks']
		);

		$vaccines[24] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "MMR (Dose 1 of ".$vaccineType['MMR']->total_dose.")",
			"protects" 		=> $vaccineType['MMR']->protect_against,
			"date" 			=> $vaccineType['MMR']->patient[1]['date'],
			"details" 		=> $vaccineType['MMR']->patient[1]['details'],
			"remarks"		=> $vaccineType['MMR']->patient[1]['remarks']
		);

		$vaccines[25] = array(
			"child_age" 	=> "12 months to 18 months",
			"vaccine_dose"  => "DTaP (Dose 4 of ".$vaccineType['DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTaP']->protect_against,
			"date" 			=> $vaccineType['DTaP']->patient[4]['date'],
			"details" 		=> $vaccineType['DTaP']->patient[4]['details'],
			"remarks"		=> $vaccineType['DTaP']->patient[4]['remarks']
		);

		$vaccines[26] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "OPV/IPV (Dose 4 of ".$vaccineType['OPV/IPV']->total_dose.")",
			"protects" 		=> $vaccineType['OPV/IPV']->protect_against,
			"date" 			=> $vaccineType['OPV/IPV']->patient[4]['date'],
			"details" 		=> $vaccineType['OPV/IPV']->patient[4]['details'],
			"remarks"		=> $vaccineType['OPV/IPV']->patient[4]['remarks']
		);

		$vaccines[27] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Hib (Dose 4 of ".$vaccineType['Hib']->total_dose.")",
			"protects" 		=> $vaccineType['Hib']->protect_against,
			"date" 			=> $vaccineType['Hib']->patient[4]['date'],
			"details" 		=> $vaccineType['Hib']->patient[4]['details'],
			"remarks"		=> $vaccineType['Hib']->patient[4]['remarks']
		);

		$vaccines[28] = array(
			"child_age" 	=> "12 months to 24 months",
			"vaccine_dose"  => "Hepa A (Dose 1 of ".$vaccineType['Hepa A']->total_dose.")",
			"protects" 		=> $vaccineType['Hepa A']->protect_against,
			"date" 			=> $vaccineType['Hepa A']->patient[1]['date'],
			"details" 		=> $vaccineType['Hepa A']->patient[1]['details'],
			"remarks"		=> $vaccineType['Hepa A']->patient[1]['remarks']
		);

		$vaccines[29] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Hepa A (Dose 2 of ".$vaccineType['Hepa A']->total_dose.")",
			"protects" 		=> $vaccineType['Hepa A']->protect_against,
			"date" 			=> $vaccineType['Hepa A']->patient[2]['date'],
			"details" 		=> $vaccineType['Hepa A']->patient[2]['details'],
			"remarks"		=> $vaccineType['Hepa A']->patient[2]['remarks']
		);

		$vaccines[30] = array(
			"child_age" 	=> "12 months and older",
			"vaccine_dose"  => "Influenza (Dose 2 of ".$vaccineType['Influenza']->total_dose.")",
			"protects" 		=> $vaccineType['Influenza']->protect_against,
			"date" 			=> $vaccineType['Influenza']->patient[2]['date'],
			"details" 		=> $vaccineType['Influenza']->patient[2]['details'],
			"remarks"		=> $vaccineType['Influenza']->patient[2]['remarks']
		);

		$vaccines[31] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "Influenza (Dose 3 of ".$vaccineType['Influenza']->total_dose.")",
			"protects" 		=> $vaccineType['Influenza']->protect_against,
			"date" 			=> $vaccineType['Influenza']->patient[3]['date'],
			"details" 		=> $vaccineType['Influenza']->patient[3]['details'],
			"remarks"		=> $vaccineType['Influenza']->patient[3]['remarks']
		);

		$vaccines[32] = array(
			"child_age" 	=> "16 months to 6 years",
			"vaccine_dose"  => "MMR (Dose 2 of ".$vaccineType['MMR']->total_dose.")",
			"protects" 		=> $vaccineType['MMR']->protect_against,
			"date" 			=> $vaccineType['MMR']->patient[2]['date'],
			"details" 		=> $vaccineType['MMR']->patient[2]['details'],
			"remarks"		=> $vaccineType['MMR']->patient[2]['remarks']
		);

		$vaccines[33] = array(
			"child_age" 	=> "18 months to 6 years",
			"vaccine_dose"  => "Varicella (Dose 2 of ".$vaccineType['Varicella']->total_dose.")",
			"protects" 		=> $vaccineType['Varicella']->protect_against,
			"date" 			=> $vaccineType['Varicella']->patient[2]['date'],
			"details" 		=> $vaccineType['Varicella']->patient[2]['details'],
			"remarks"		=> $vaccineType['Varicella']->patient[2]['remarks']
		);

		$vaccines[34] = array(
			"child_age" 	=> "4 years to 6 years",
			"vaccine_dose"  => "DTaP (Dose 5 of ".$vaccineType['DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTaP']->protect_against,
			"date" 			=> $vaccineType['DTaP']->patient[5]['date'],
			"details" 		=> $vaccineType['DTaP']->patient[5]['details'],
			"remarks"		=> $vaccineType['DTaP']->patient[5]['remarks']
		);

		$vaccines[35] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "OPV/IPV (Dose 5 of ".$vaccineType['OPV/IPV']->total_dose.")",
			"protects" 		=> $vaccineType['OPV/IPV']->protect_against,
			"date" 			=> $vaccineType['OPV/IPV']->patient[5]['date'],
			"details" 		=> $vaccineType['OPV/IPV']->patient[5]['details'],
			"remarks"		=> $vaccineType['OPV/IPV']->patient[5]['remarks']
		);

		$vaccines[36] = array(
			"child_age" 	=> "7 years and older",
			"vaccine_dose"  => "DTwP/DTaP (Dose 1 of ".$vaccineType['DTwP/DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTwP/DTaP']->protect_against,
			"date" 			=> $vaccineType['DTwP/DTaP']->patient[1]['date'],
			"details" 		=> $vaccineType['DTwP/DTaP']->patient[1]['details'],
			"remarks"		=> $vaccineType['DTwP/DTaP']->patient[1]['remarks']
		);

		$vaccines[37] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "DTwP/DTaP (Dose 2 of ".$vaccineType['DTwP/DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTwP/DTaP']->protect_against,
			"date" 			=> $vaccineType['DTwP/DTaP']->patient[2]['date'],
			"details" 		=> $vaccineType['DTwP/DTaP']->patient[2]['details'],
			"remarks"		=> $vaccineType['DTwP/DTaP']->patient[2]['remarks']
		);

		$vaccines[38] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "DTwP/DTaP (Dose 3 of ".$vaccineType['DTwP/DTaP']->total_dose.")",
			"protects" 		=> $vaccineType['DTwP/DTaP']->protect_against,
			"date" 			=> $vaccineType['DTwP/DTaP']->patient[3]['date'],
			"details" 		=> $vaccineType['DTwP/DTaP']->patient[3]['details'],
			"remarks"		=> $vaccineType['DTwP/DTaP']->patient[3]['remarks']
		);

		$vaccines[39] = array(
			"child_age" 	=> "9 years and older",
			"vaccine_dose"  => "HPV (Dose 1 of ".$vaccineType['HPV']->total_dose.")",
			"protects" 		=> $vaccineType['HPV']->protect_against,
			"date" 			=> $vaccineType['HPV']->patient[1]['date'],
			"details" 		=> $vaccineType['HPV']->patient[1]['details'],
			"remarks"		=> $vaccineType['HPV']->patient[1]['remarks']
		);

		$vaccines[40] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "HPV (Dose 2 of ".$vaccineType['HPV']->total_dose.")",
			"protects" 		=> $vaccineType['HPV']->protect_against,
			"date" 			=> $vaccineType['HPV']->patient[2]['date'],
			"details" 		=> $vaccineType['HPV']->patient[2]['details'],
			"remarks"		=> $vaccineType['HPV']->patient[2]['remarks']
		);

		$vaccines[41] = array(
			"child_age" 	=> "",
			"vaccine_dose"  => "HPV (Dose 3 of ".$vaccineType['HPV']->total_dose.")",
			"protects" 		=> $vaccineType['HPV']->protect_against,
			"date" 			=> $vaccineType['HPV']->patient[3]['date'],
			"details" 		=> $vaccineType['HPV']->patient[3]['details'],
			"remarks"		=> $vaccineType['HPV']->patient[3]['remarks']
		);

		return $vaccines;
	}
}
