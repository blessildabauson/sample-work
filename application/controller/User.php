<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('UserTypeModel');
	}

	public function dump($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}

	public function index() {
		if($this->session->userdata('logged_in')) {
            redirect('/dashboard', 'refresh');
        } else {
            $this->load->view('login');
        }

	}

	public function login() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

		if ($this->form_validation->run()) {
			redirect('/dashboard', 'refresh');
		} else {
			$this->load->view('login');
		}
	}

	public function logout() {
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}

	public function check_database($password) {
   		$username = $this->input->post('username');
   		$result   = $this->UserModel->login($username, $password);
   		if($result && is_array($result) && count($result) && is_object($result[0])) {
			$this->session->set_userdata('logged_in', $result[0]);
     		return true;
   		} else {
     		$this->form_validation->set_message('check_database', 'Invalid username or password');
     		return false;
   		}
 	}

 	public function staff(){
 		$data['user']  	= $this->session->userdata('logged_in');
 		$data['staff'] 	= $this->UserModel->getAll();
 		$utypes 		= $this->UserTypeModel->getAll();

 		foreach($utypes as $val){
 		 	$data['user_types'][$val->user_type_id] = $val->label;
 		}

 		$this->load->view('staff.php', $data);
 	}

 	public function add_staff(){
 		$data['user']  = $this->session->userdata('logged_in');
 		if($_SERVER['REQUEST_METHOD'] == "POST"){
 			$staff = $this->UserModel->create([
		                'email'      	=> $_POST['email'],
		                'username'   	=> $_POST['username'],
		                'password'      => md5($_POST['password']),
		                'dname'    		=> $_POST['dname'],
		                'active'     	=> $_POST['active'],
		                'user_type_id'  => $_POST['user_type_id'],
		                'date_added'  	=> date("Y-m-d H:i:s")
		            ]);
 			echo "Success";
 		}
 	}

 	public function edit_staff($user_id = 0){
 		$data['user']  	  = $this->session->userdata('logged_in');
 		$data['staff']    = $this->UserModel->getOneWhere('user_id', $user_id);
 		$data['staff_id'] = $user_id;

 		$utypes = $this->UserTypeModel->getAll();
 		foreach($utypes as $val){
 		 	$data['user_types'][$val->user_type_id] = $val->label;
 		}

 		$this->load->view('edit_staff.php', $data);

 		if($_SERVER['REQUEST_METHOD'] == "POST"){
 			$uarr = array(
                'user_id' 		=> $_POST['user_id'],
                'email'      	=> $_POST['email'],
		        'username'   	=> $_POST['username'],
		        'dname'    		=> $_POST['dname'],
		        'active'     	=> $_POST['active'],
		        'user_type_id'  => $_POST['user_type_id']
            );
            if(!empty($_POST['password'])){
            	$uarr['password'] = md5($_POST['password']);
            }
            $this->UserModel->updateDB($uarr);
            $this->session->set_flashdata('success', 'Successfully updated staff.');
            redirect('/user/staff', 'refresh');
 		}
 	}

 	public function delete_staff($user_id = 0){
        if($this->session->userdata('logged_in') && !empty($user_id)) {
            $this->UserModel->delete($user_id);
            $this->session->set_flashdata('success', 'Successfully deleted staff.');
            redirect('/user/staff', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }
}