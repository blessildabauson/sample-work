<?php
	require_once APPPATH . 'models/data_access_layer.php';

	Class ConsultationModel extends Data_Access_Layer {

	    const TABLE_NAME 	= 'consultation';
	    const PRIMARY_KEY 	= 'consultation_id';

	    protected $_jsonFields     = [];
	    protected $_validations    = [];

	    public function __construct() {
	        parent::__construct();
	    }

	    public function dump($arr){
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        }

        public function getAllConsultations($post = array()){

        	$this->db->select('c.*, p.last_name, p.first_name');
			$this->db->from('consultation AS c');
			$this->db->join('patient_consultation AS pc', 'c.consultation_id = pc.consultation_id', 'left');
			$this->db->join('patient AS p', 'p.patient_id = pc.patient_id', 'left');

			if(count($post) > 0){
				if(isset($_POST['type_visit']) && !empty($_POST['type_visit'])){
					$this->db->where('c.type_visit', $_POST['type_visit']);
				}
				if(isset($_POST['date_visit']) && !empty($_POST['date_visit'])){
					$this->db->where('c.date_visit', $_POST['date_visit']);
				}
				if(isset($_POST['patient_name']) && !empty($_POST['patient_name'])){
					$this->db->where('p.last_name LIKE "%'.$_POST['patient_name'].'%" OR p.first_name LIKE "%'.$_POST['patient_name'].'%" ');
				}
				if(isset($_POST['diagnosis']) && !empty($_POST['diagnosis'])){
					$this->db->where('c.diagnosis LIKE "%'.$_POST['diagnosis'].'%"');
				}
			}
			$this->db->order_by("c.date_visit DESC");

			$query  = $this->db->get();
			$result = $query->result();

			return $result;
        }

        
        public function delete($id = 0){
            if(!empty($id)){
                $query = $this->db->query("DELETE FROM consultation WHERE consultation_id = ".$id);
            }
        }
	}
?>
