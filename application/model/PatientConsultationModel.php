<?php
	require_once APPPATH . 'models/data_access_layer.php';

	Class PatientConsultationModel extends Data_Access_Layer {

	    const TABLE_NAME 	= 'patient_consultation';
	    const PRIMARY_KEY 	= 'patient_consultation_id';

	    protected $_jsonFields     = [];
	    protected $_validations    = [];

	    public function __construct() {
	        parent::__construct();
	    }

	    public function dump($arr){
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        }

        public function getScheduledPatientsToday(){
        	$this->db->select('p.last_name, p.middle_name, p.first_name, c.reason_next_visit');
            $this->db->from('patient_consultation AS pc');
            $this->db->join('patient AS p', 'pc.patient_id = p.patient_id', 'left');
            $this->db->join('consultation AS c', 'pc.consultation_id = c.consultation_id', 'left');
            $this->db->where("c.next_visit = '". date("Y-m-d")."'");

            $query  = $this->db->get();
            $result = $query->result();
            return $result;
        }

        public function getPatientConsulations($patient_id = 0){
            $this->db->select('c.*');
            $this->db->from('patient_consultation AS pc');
            $this->db->join('patient AS p', 'pc.patient_id = p.patient_id', 'left');
            $this->db->join('consultation AS c', 'pc.consultation_id = c.consultation_id', 'left');
            $this->db->where("pc.patient_id = ". $patient_id);

            $query  = $this->db->get();
            $result = $query->result();
            return $result;
        }
	   
	}
?>
