<?php
	require_once APPPATH . 'models/data_access_layer.php';

	Class PatientMedicationModel extends Data_Access_Layer {

	    const TABLE_NAME 	= 'patient_medication';
	    const PRIMARY_KEY 	= 'patient_medication_id';

	    protected $_jsonFields     = [];
	    protected $_validations    = [];

	    public function __construct() {
	        parent::__construct();
	    }

	    public function dump($arr){
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        }

        public function getPatientMedications($patient_id = 0){
            $this->db->select('pm.*,i.item_name');
            $this->db->from('patient_medication AS pm');
            $this->db->join('patient AS p', 'pm.patient_id = p.patient_id', 'left');
            $this->db->join('inventory AS i', 'pm.inventory_id = i.inventory_id', 'left');
            $this->db->where("pm.patient_id = ". $patient_id);

            $query  = $this->db->get();
            $result = $query->result();
            return $result;
        }

	   
	}
?>

