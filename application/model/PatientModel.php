<?php
	require_once APPPATH . 'models/data_access_layer.php';

	Class PatientModel extends Data_Access_Layer {

	    const TABLE_NAME 	= 'patient';
	    const PRIMARY_KEY 	= 'patient_id';

	    protected $_jsonFields     = [];
	    protected $_validations    = [];

	    public function __construct() {
	        parent::__construct();
	    }

	    public function dump($arr){
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        }

        public function record_count($post = array()) {
			$this->db->select('*');
			$this->db->from('patient');
			
			if(count($post) > 0 && $_POST['action'] == "filter"){
				if(isset($_POST['clinic_id']) && !empty($_POST['clinic_id'])){
					$this->db->where('clinic_id', $_POST['clinic_id']);
				}
				if(isset($_POST['cabinet_code']) && !empty($_POST['cabinet_code'])){
					$this->db->where('cabinet_code', $_POST['cabinet_code']);
				}
				if(isset($_POST['type']) && !empty($_POST['type'])){
					$this->db->where('type', $_POST['type']);
				}
				if(isset($_POST['archive']) && !empty($_POST['archive'])){
					$this->db->where('archive', $_POST['archive']);
				}
				if(isset($_POST['name']) && !empty($_POST['name'])){
					$this->db->where('last_name LIKE "%'.$_POST['name'].'%" OR first_name LIKE "%'.$_POST['name'].'%"');
				}
				if(isset($_POST['guardian']) && !empty($_POST['guardian'])){
					$this->db->where('guardian LIKE "%'.$_POST['guardian'].'%"');
				}
			}else{
				$this->db->where('archive', 0);
			}

			$query  = $this->db->get();
			$result = $query->result();

			return count($result);
		}


        public function getPatientsList($limit = 0, $offset = 0, $post = array()){

        	$this->db->limit($limit, $offset);
        	$this->db->select('patient_id,clinic_id, cabinet_code, file_no, last_name, first_name, guardian, phone_no, mobile_no, last_visit');
			$this->db->from('patient');
			
			if(count($post) > 0 && $_POST['action'] == "filter"){
				if(isset($_POST['clinic_id']) && !empty($_POST['clinic_id'])){
					$this->db->where('clinic_id', $_POST['clinic_id']);
				}
				if(isset($_POST['cabinet_code']) && !empty($_POST['cabinet_code'])){
					$this->db->where('cabinet_code', $_POST['cabinet_code']);
				}
				if(isset($_POST['type']) && !empty($_POST['type'])){
					$this->db->where('type', $_POST['type']);
				}
				if(isset($_POST['archive']) && !empty($_POST['archive'])){
					$this->db->where('archive', $_POST['archive']);
				}
				if(isset($_POST['name']) && !empty($_POST['name'])){
					$this->db->where('last_name LIKE "%'.$_POST['name'].'%" OR first_name LIKE "%'.$_POST['name'].'%"');
				}
				if(isset($_POST['guardian']) && !empty($_POST['guardian'])){
					$this->db->where('guardian LIKE "%'.$_POST['guardian'].'%"');
				}
			}else{
				$this->db->where('archive', 0);
			}
			$this->db->order_by("last_name");

			$query  = $this->db->get();
			$result = $query->result();

			return $result;
        }

        public function delete($user_id = 0){
            if(!empty($user_id)){
                $query = $this->db->query("DELETE FROM patient WHERE patient_id = ".$user_id);
            }
        }
	}
?>
