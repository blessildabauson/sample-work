<?php
	require_once APPPATH . 'models/data_access_layer.php';

	Class UserModel extends Data_Access_Layer {

	    const TABLE_NAME 	= 'user';
	    const PRIMARY_KEY 	= 'user_id';


	    protected $_jsonFields     = [];
	    protected $_validations    = [];
        protected $_tableRelations = false;

        public function __construct($user_id = false) {
            parent::__construct($user_id, true);
        }

        public function dump($arr){
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        }

        public function login($username, $password) {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('username', $username);
            $this->db->where('password', MD5($password));
            $this->db->limit(1);

            $query = $this->db->get();
            if($query->num_rows() == 1)
                return $this->getOne($query->result()[0]->user_id, true);
            return false;
        }
	   
        public function delete($user_id = 0){
            if(!empty($user_id)){
                $query = $this->db->query("DELETE FROM user WHERE user_id = ".$user_id);
            }
        }
	}
?>
