<?php
	require_once APPPATH . 'models/data_access_layer.php';

	Class UserTypeModel extends Data_Access_Layer {

	    const TABLE_NAME 	= 'user_type';
	    const PRIMARY_KEY 	= 'user_type_id';

	    protected $_jsonFields     = [];
	    protected $_validations    = [];

	    public function __construct() {
	        parent::__construct();
	    }

	    public function dump($arr){
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        }
	   
	}
?>
