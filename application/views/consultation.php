<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php ob_start();?>

<form id="filterForm" method="POST" action="/consultation">
	<input type="hidden" name="action" value="filter" />
	<section class="panel">
		<header class="panel-heading">
	        <div class="panel-actions">
	            <a href="#" class="fa fa-caret-down"></a>
	            <a href="#" class="fa fa-times"></a>
	        </div>
	        <p class="panel-title">
	            <h4>Filter results by:</h4>
	        </p>
	    </header>
	    <div class="panel-body">
	    	<fieldset>
	        	<div class="form-group">
	        		<div class="col-md-6">
		                <label class="col-md-4 control-label" for="textinput">Visit Type</label>  
		                <div class="col-md-8">
		                    <select name="type_visit" class="form-control">
		                    	<option value="">Select from list</option>
		                    	<option value="Check Up" <?php echo (count($post) > 0 && isset($post['type_visit']) && $post['type_visit'] == "Check Up") ? "selected":"";?> >Check Up</option>
		                    	<option value="Follow Up" <?php echo (count($post) > 0 && isset($post['type_visit']) && $post['type_visit'] == "Follow Up") ? "selected":"";?> >Follow Up</option>
		                    	<option value="Vaccination" <?php echo (count($post) > 0 && isset($post['type_visit']) && $post['type_visit'] == "Vaccination") ? "selected":"";?> >Vaccination</option>
		                    </select>
		                </div>
		            </div>
		            <div class="col-md-6">
		            	<label class="col-md-4 control-label" for="textinput">Date of Visit</label>  
		                <div class="col-md-8">
		                    <input name="date_visit" type="text" class="form-control" value="<?php echo (count($post) && isset($post['date_visit'])) ? $post['date_visit']:"";?>">
		                </div>
		            </div>
	            </div>
	            <div class="form-group">
	            	<div class="col-md-6">
		                <label class="col-md-4 control-label" for="textinput">Patient Name</label>  
		                <div class="col-md-8">
		                    <input name="patient_name" type="text" class="form-control" value="<?php echo (count($post) && isset($post['patient_name'])) ? $post['patient_name']:"";?>">
		                </div>
		            </div>
		            <div class="col-md-6">
		            	<label class="col-md-4 control-label" for="textinput">Diagnosis Keyword</label>  
		                <div class="col-md-8">
		                    <input name="diagnosis" type="text" class="form-control" value="<?php echo (count($post) && isset($post['diagnosis'])) ? $post['diagnosis']:"";?>">
		                </div>
		            </div>
	            </div>
	            <div class="form-group">
	            	<button type="submit" class="btn btn-primary">Filter</button>
	            </div>
	        </fieldset>
	    </div>
	</section>
</form>

<section class="panel">
	<header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            <a href="#" class="fa fa-times"></a>
        </div>
        <p class="panel-title"></p>
    </header>
    <div class="panel-body">
    	<table class="table" id="listData">
            <input type="hidden" id="toUpdateId" value="" />
            <thead>
                <tr>
                	<th class="text-center" style="width:70px;">Visit</th>
                    <th class="text-left" style="width:150px;">Patient</th>
                    <th class="text-center" style="width:50px;">Weight (kg)</th>
                    <th class="text-left" style="width:150px;">Private Notes</th>
                    <th class="text-left" style="width:150px;">Diagnosis</th>
                    <th class="text-center" style="width:100px;">Date</th>
                    <?php if($user->user_type_id == 1) {?>
                         <th class="text-center" style="width:50px;">Action</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php if(count($consultations)){ ?>
                    <?php foreach($consultations as $val){ ?>
                       <tr>
                            <td class="text-center"><?php echo $val->type_visit;?></td>
                            <td><?php echo $val->last_name." ".$val->first_name;?></td>
                            <td class="text-center"><?php echo $val->weight;?></td>
                            <td><?php echo $val->private_notes;?></td>
                            <td><?php echo $val->diagnosis;?></td>
                            <td class="text-center"><?php echo $val->date_visit;?></td>
                            <?php if($user->user_type_id == 1) {?>
                                <td class="text-center">
                                    <a data-href="/consultation/delete/<?php echo $val->consultation_id;?>" data-toggle="modal" data-target="#deleteModal" title='Delete Consultation' onCLick="addId(<?php echo $val->consultation_id;?>)">
                                        <span class='fa fa-trash'></span>
                                    </a>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>              
        </table>
    </div>
</section>

<div id='deleteModal' class="modal modal-danger fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><?php echo gettext("Confirm Deletion");?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo gettext("Are you sure you want to delete?");?></p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="toDeleteId" value="" />
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><?php echo gettext("Close");?></button>
                <button id="confirm_delete" type="button" class="btn btn-danger btn-ok"><?php echo gettext("Confirm Deletion");?></button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#listData').DataTable();
        $('#deleteModal').on('show.bs.modal', function(e) {
            $("#confirm_delete").click(function(){
                var href = "/consultation/delete/"+ $("#toUpdateId").val();
                $.post(href).done(function(response) {
                   window.location.href = '/consultation'
                });
            });
        });
    });
    function bz_message_fade(type, target, message, duration) {
        $(target).addClass(type);
        $(target).html(message);
        $(target).fadeIn('slow');
        setTimeout(function () {
            $(target).fadeOut('slow');
        }, duration);
    }
    
    function addId(id){
        $("#toUpdateId").val(id);
    }
</script>

<?php
    $content = ob_get_clean();
    $template = $this->load->view('inc/main_template.php', [
        'title'       => "Consultations",
        'pagetitle'   => "Consultations",
        'breadcrumbs' => [
            [
                'link'  => '/consultation',
                'title' => "Consultations"
            ]
        ],
        'section' => "Consultation",
        'content' => $content
    ]);
?>