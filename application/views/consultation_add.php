<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php ob_start();?>

<table id="detailTbl" class="table">
  	<tbody>
	  	<tr>
	  		<th class="text-center">Code</th>
	  		<th class="text-center">File No.</th>
	  		<th class="text-center">Patient Name</th>
	  		<th class="text-center">Gender</th>
	  		<th class="text-center">Birthday</th>
	  		<th class="text-center">Guardian</th>
	  		<th>&nbsp;</th>
	  	</tr>
	  	<tr>
	  		<td class="text-center"><?php echo $patient->cabinet_code;?></td>
	  		<td class="text-center"><?php echo $patient->file_no;?></td>
	  		<td class="text-center">
	  			<?php echo $patient->last_name." ".$patient->first_name." ".$patient->middle_name;?>
	  		</td>
	  		<td class="text-center"><?php echo $patient->gender;?></td>
	  		<td class="text-center"><?php echo $patient->dob;?></td>
	  		<td class="text-center"><?php echo $patient->guardian;?> (<?php echo $patient->guardian_relationship;?>)</td>
				<td class="text-center" style="width:110px;">
	  			<div class="dropdown">
				    <a class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks"></span></a>
				    <ul class="dropdown-menu dropdown-menu-left">
				      <li><a href="/patient/edit/<?php echo $patient_id;?>"><span class="glyphicon glyphicon-edit"></span> Edit Patient Details</a></li>
				      <li><a href="/patient/record/<?php echo $patient_id;?>"><span class="glyphicon glyphicon-folder-open"></span> &nbsp;View Record</a></li>
				      <li><a href="#"><span class="glyphicon glyphicon-upload"></span> Upload Medical Records</a></li>
				      <li><a href="#"><span class="glyphicon glyphicon-duplicate"></span> Patient Medical Certificates</a></li>
				      <?php if($user->user_type_id == 1) {?>
				      		<li><a href="/patient/delete/<?php echo $patient_id;?>"><span class='fa fa-trash'></span> Delete Patient</a></li>
				      <?php } ?>
				    </ul>
				</div>
	  		</td>
	  	</tr>
  	</tbody>
</table><br/>

<form id="addForm" method="POST" action="/consultation/add">
	<input type="hidden" name="patient_id" value="<?php echo $patient_id;?>" />
	<section class="panel">
		<header class="panel-heading">
	        <div class="panel-actions">
	            <a href="#" class="fa fa-caret-down"></a>
	            <a href="#" class="fa fa-times"></a>
	        </div>
	        <p class="panel-title">
	        	<h4>Patient Consultation Record</h4>
	        </p>
	    </header>
	    <div class="panel-body">
	    	<fieldset>
	    		<div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Type of visit</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="type_visit">
                            	<option value="">Select from the list</option>
							  	<option value="Check Up">Check Up</option>
							  	<option value="Follow Up">Follow Up</option>
							  	<option value="Vaccination">Vaccination</option>
							</select>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<label class="col-md-4 control-label" for="textinput">Date of Visit:</label>
                    	<div class="col-md-8">
                            <div id="bdaypicker" class="input-group date" data-provide="datepicker">
							    <input type="text" class="form-control" name="date_visit" value="<?php echo date("Y-m-d");?>">
							    <div class="input-group-addon">
							        <span class="fa fa-calendar"></span>
							    </div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Weight (kg)</label>  
                        <div class="col-md-8">
                            <input name="weight" type="text" class="form-control">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Height (cm)</label>  
                        <div class="col-md-8">
                            <input name="heights" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">BMI (bmi)</label>  
                        <div class="col-md-8">
                            <input name="bmi" type="text" class="form-control">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Temperature (&#8451;)</label>  
                        <div class="col-md-8">
                            <input name="temperature" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-12">
                    	<label class="col-md-2 control-label">Diagnosis</label>  
                        <div class="col-md-10">
                            <textarea class="form-control" rows="3" name="diagnosis"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-12">
                    	<label class="col-md-2 control-label">Private Notes</label>  
                        <div class="col-md-10">
                            <textarea class="form-control" rows="3" name="private_notes"></textarea>
                        </div>
                    </div>
                </div>

                <hr/>
			    <a href="javascript:void(0)" data-toggle="collapse" data-target="#vaccine-panel">
			    	Vaccinations Administered <span class="glyphicon glyphicon-menu-down"></span>
			    </a><br/>
			    <div id="vaccine-panel" class="collapse info-panel" style="margin-top:20px;">
			    	<input type="hidden" id="vaccineCnt" value="1" />
			    	<div id="vaclist">
				    	<div id="vaccines_1" class="form-group">
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
	                            <select class="form-control" name="vaccine[]">
	                            	<option value="">Vaccine</option>
	                            	<?php foreach($vaccine_kinds as $val1) { ?>
								  		<option value="<?php echo $val1->vaccination_kind_id?>"><?php echo $val1->label?></option>
								  	<?php } ?>
								</select>
	                        </div>
	                        <div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
	                            <select class="form-control" name="dose[]">
	                            	<option value="">Dose</option>
								  	<option value="1">1</option>
								  	<option value="2">2</option>
								  	<option value="3">3</option>
								  	<option value="4">4</option>
								  	<option value="5">5</option>
								  	<option value="Booster">Booster</option>
								  	<option value="Catch-up:1">Catch-up:1</option>
								  	<option value="Catch-up:2">Catch-up:2</option>
								  	<option value="Catch-up:3">Catch-up:3</option>
								  	<option value="Catch-up:4">Catch-up:4</option>
								  	<option value="Catch-up:5">Catch-up:5</option>
								</select>
	                        </div>
	                        <div class="col-md-3" style="padding-right: 2px;padding-left: 2px;">
	                            <select class="form-control" name="v_inventory_id[]">
	                            	<option value="0">Inventory Item</option>
	                            	<?php foreach($inv_vacs as $val1) { ?>
								  		<option value="<?php echo $val1->inventory_id?>"><?php echo $val1->item_name?> (<?php echo $val1->generic_name?>)</option>
								  	<?php } ?>
								</select>
	                        </div>
	                        <div class="col-md-2" style="padding-right:2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Brand" name="brand[]">
			    			</div>
			    			<div class="col-md-2" style="padding-right:2px;padding-left: 2px;">
								<input type="text" class="form-control" placeholder="Batch" name="batch_no[]">
							</div>
							<div class="col-md-7" style="padding-right:2px;padding-left: 2px;">
						  		<input type="text" class="form-control" placeholder="Remarks" name="remarks[]">
						  	</div>
						  	<div class="col-md-2" style="padding-right:2px;padding-left: 2px;">
						  		<div id="bdaypicker" class="input-group date givenpicker" data-provide="datepicker">
								    <input type="text" class="form-control" name="date_given[]" placeholder="Date Given">
								    <div class="input-group-addon">
								        <span class="fa fa-calendar"></span>
								    </div>
								</div>
						    </div>
						    <div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
						    	<div id="bdaypicker2" class="input-group date nextpicker" data-provide="datepicker">
								    <input type="text" class="form-control" name="next_sched[]" placeholder="Next Sched">
								    <div class="input-group-addon">
								        <span class="fa fa-calendar"></span>
								    </div>
								</div>
					        </div>
					        <a id="removeVacBtn" href="javascript:void(0)" onclick="removeVaccine(1);" style="vertical-align:middle;">
					        	<i class="fa fa-times" aria-hidden="true"></i>
					        </a>
				    	</div>
				    </div><br/><br/>
			    	<button type="button" class="btn" onclick="addNewVaccine()">Add more vaccines</button>
			    </div>

			    <hr/>
			    <a href="javascript:void(0)" data-toggle="collapse" data-target="#meds-panel">
			    	Medications Prescribed <span class="glyphicon glyphicon-menu-down"></span>
			    </a><br/>
			    <div id="meds-panel" class="collapse meds-panel" style="margin-top:20px;">
			    	<input type="hidden" id="medCnt" value="1" />
			    	<div id="medlist">
			    		<div id="meds_1" class="form-group">
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<select class="form-control" name="m_inventory_id[]">
			    					<option value="0">Inventory Item</option>
	                            	<?php foreach($inv_meds as $val1) { ?>
								  		<option value="<?php echo $val1->inventory_id?>"><?php echo $val1->item_name?> (<?php echo $val1->generic_name?>)</option>
								  	<?php } ?>
			    				</select>
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Generic" name="generic_name[]">
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Dose" name="med_dose[]">
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<select class="form-control" name="dose_unit[]">
	                            	<option value="">Dose</option>
								  	<option value="ml">ml</option>
								  	<option value="mg">mg</option>
								  	<option value="drops">drops</option>
								  	<option value="capsule">capsule</option>
								  	<option value="tablet">tablet</option>
								  	<option value="puffs">puffs</option>
								  	<option value="tablespoon">tablespoon</option>
								  	<option value="teaspoon">teaspoon</option>
								  	<option value="ounces">ounces</option>
								  	<option value="pints">pints</option>
								</select>
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Quantity" name="quantity[]">
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Stock" name="stock_dose[]">
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Frequency" name="frequency[]">
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Duration" name="duration[]">
			    			</div>
			    			<div class="col-md-2" style="padding-right: 2px;padding-left: 2px;">
			    				<select class="form-control" name="duration_unit[]">
	                            	<option value="">Duration Unit</option>
								  	<option value="days">days</option>
								  	<option value="weeks">weeks</option>
								  	<option value="months">months</option>
								  	<option value="years">years</option>
								</select>
			    			</div>
			    			<div class="col-md-5" style="padding-right: 2px;padding-left: 2px;">
			    				<input type="text" class="form-control" placeholder="Instructions" name="instruction[]">
			    			</div>
			    			<a id="removeMedBtn" href="javascript:void(0)" onclick="removeMedicine(1);" style="vertical-align:middle;">
					        	<i class="fa fa-times" aria-hidden="true"></i>
					        </a>
			    		</div>
			    	</div><br/><br/>
			    	<button type="button" class="btn" onclick="addNewMedicine()">Add more medicines</button>
			    </div><br/><br/>
			    
			    <div class="form-group">
                	<div class="col-md-12">
                    	<label class="col-md-2 control-label">Other Instructions</label>  
                        <div class="col-md-10">
                            <textarea class="form-control" rows="3" name="other_instruction"></textarea>
                        </div>
                    </div>
                </div>
			    <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Next Visit</label>
                    	<div class="col-md-8">
                            <div id="nextvisit" class="input-group date" data-provide="datepicker">
							    <input type="text" class="form-control" name="next_visit">
							    <div class="input-group-addon">
							        <span class="fa fa-calendar"></span>
							    </div>
							</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<label class="col-md-4 control-label">Reason for next visit</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="3" name="reason_next_visit"></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
		           	<button type="submit" class="btn btn-primary">Submit</button>
		        </div>

	    	</fieldset>
	    </div>
	</section>
</form>

<script>
    $(document).ready(function() {
        $('#bdaypicker').datepicker({
    		format: "yyyy-mm-dd",
		    todayBtn: true,
		    todayHighlight: true,
		    orientation: "auto bottom"
		});
		$('.nextpicker').datepicker({
    		format: "yyyy-mm-dd",
		    todayBtn: true,
		    todayHighlight: true,
		    orientation: "auto bottom"
		});
		$('.givenpicker').datepicker({
    		format: "yyyy-mm-dd",
		    todayBtn: true,
		    todayHighlight: true,
		    orientation: "auto bottom"
		});
		$('#nextvisit').datepicker({
    		format: "yyyy-mm-dd",
		    todayBtn: true,
		    todayHighlight: true,
		    orientation: "auto bottom"
		});
    });

    function removeVaccine(cnt){
    	$("#vaccines_"+cnt).remove();
    }

    function removeMedicine(cnt){
    	$("#meds_"+cnt).remove();
    }

    function addNewMedicine(){
    	var cur_cnt = $("#medCnt").val();
    	var new_cnt	= parseInt(cur_cnt) + 1;
    	var row_str = $("#meds_"+cur_cnt).html();

    	row_str = '<div id="meds_'+new_cnt+'" class="form-group">' + row_str.replace("meds_"+cur_cnt, "meds_"+new_cnt) + '</div>';
    	row_str = row_str.replace("removeMedicine("+cur_cnt+")", "removeMedicine("+new_cnt+")");

    	$("#medlist").append(row_str);
    	$("#medCnt").val(new_cnt);
    }

    function addNewVaccine(){
    	var cur_cnt = $("#vaccineCnt").val();
    	var new_cnt	= parseInt(cur_cnt) + 1;
    	var row_str = $("#vaccines_"+cur_cnt).html();

    	row_str = '<div id="vaccines_'+new_cnt+'" class="form-group">' + row_str.replace("vaccines_"+cur_cnt, "vaccines_"+new_cnt) + '</div>';
    	row_str = row_str.replace("removeVaccine("+cur_cnt+")", "removeVaccine("+new_cnt+")");
    	
    	$("#vaclist").append(row_str);
    	$("#vaccineCnt").val(new_cnt);
    }
</script>

<?php
    $content = ob_get_clean();
    $template = $this->load->view('inc/main_template.php', [
        'title'       => "Consultation",
        'pagetitle'   => "Add Consultation",
        'breadcrumbs' => [
            [
                'link'  => '/consultation',
                'title' => "Consultation"
            ],
            [
                'link'  => '/consultation/add/'.$patient_id,
                'title' => "Add"
            ]
        ],
        'section' => "Consultation",
        'content' => $content
    ]);
?>