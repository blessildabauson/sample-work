<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php ob_start();?>

<form id="filterForm" method="POST" action="/patient/lists">
	<input type="hidden" name="action" value="filter" />
	<section class="panel">
		<header class="panel-heading">
	        <div class="panel-actions">
	            <a href="#" class="fa fa-caret-down"></a>
	            <a href="#" class="fa fa-times"></a>
	        </div>
	        <p class="panel-title">
	            <h4>Filter results by:</h4>
	        </p>
	    </header>
	    <div class="panel-body">
	    	<fieldset>
                <div class="form-group">
                    <div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Patient Name</label>  
                        <div class="col-md-8">
                            <input name="name" type="text" class="form-control" value="<?php echo (count($post) && isset($post['name'])) ? $post['name']:"";?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Guardian Name</label>  
                        <div class="col-md-8">
                            <input name="guardian" type="text" class="form-control" value="<?php echo (count($post) && isset($post['guardian'])) ? $post['guardian']:"";?>">
                        </div>
                    </div>
                </div>
	        	<div class="form-group">
	        		<div class="col-md-6">
		                <label class="col-md-4 control-label" for="textinput">Clinic</label>  
		                <div class="col-md-8">
		                    <select name="clinic_id" class="form-control">
		                    	<option value="0">Select from list</option>
		                    	<?php foreach($clinics as $ckey => $cval) { ?>
		                    		<option value="<?php echo $cval->clinic_id?>" <?php echo (count($post) > 0 && isset($post['clinic_id']) && $post['clinic_id'] == $cval->clinic_id) ? "selected":"";?> >
		                    			<?php echo $cval->cname?>
		                    		</option>
		                    	<?php } ?>
		                    </select>
		                </div>
		            </div>
		            <div class="col-md-6">
		            	<label class="col-md-4 control-label" for="textinput">Cabinet Code</label>  
		                <div class="col-md-8">
		                    <select name="cabinet_code" class="form-control">
		                    	<option value="0">Select from list</option>
		                    	<option value="A" <?php echo (count($post) > 0 && isset($post['cabinet_code']) && $post['cabinet_code'] == "A") ? "selected":"";?> >A</option>
		                    	<option value="B" <?php echo (count($post) > 0 && isset($post['cabinet_code']) && $post['cabinet_code'] == "B") ? "selected":"";?> >B</option>
		                    	<option value="C" <?php echo (count($post) > 0 && isset($post['cabinet_code']) && $post['cabinet_code'] == "C") ? "selected":"";?> >C</option>
		                    </select>
		                </div>
		            </div>
	            </div>
	            <div class="form-group">
	            	<div class="col-md-6">
		                <label class="col-md-4 control-label" for="textinput">User Type</label>  
		                <div class="col-md-8">
		                    <select name="type" class="form-control">
		                    	<option value="0">Select from list</option>
		                    	<option value="1" <?php echo (count($post) > 0 && isset($post['type']) && $post['type'] == "1") ? "selected":"";?> >Adult</option>
		                    	<option value="2" <?php echo (count($post) > 0 && isset($post['type']) && $post['type'] == "2") ? "selected":"";?> >0 - 20 years old</option>
		                    </select>
		                </div>
		            </div>
		            <div class="col-md-6">
		            	<label class="col-md-4 control-label" for="textinput">Show Only Archived Patients</label>  
		                <div class="col-md-8">
		                    <input type="radio" name="archive" value="1" <?php echo (isset($post['archive']) && $post['archive'] == "1") ? "checked":"";?> > Yes &nbsp;&nbsp;
		                    <input type="radio" name="archive" value="0" <?php echo (isset($post['archive']) && $post['archive'] == "0") ? "checked":"";?> > No
		                </div>
		            </div>
	            </div>
	            <div class="form-group">
	            	<button type="submit" class="btn btn-primary">Filter</button>
	            </div>
	        </fieldset>
	    </div>
	</section>
</form>

<section class="panel">
	<header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            <a href="#" class="fa fa-times"></a>
        </div>
        <p class="panel-title">
            <a class="btn btn-default" id="add" href="/patient/add">
                <span><i class="fa fa-user-plus" aria-hidden="true"></i> &nbsp; New Patient</span>
            </a>
        </p>
    </header>
    <div class="panel-body">
    	<table class="table" id="listData">
    		<input type="hidden" id="toUpdateId" value="" />
    		<input type="hidden" id="toDeleteId" value="" />
            <thead>
                <tr>
                    <th class="text-center" style="width:50px;">Code</th>
                    <th class="text-center" style="width:70px;">File No</th>
                    <th class="text-left" style="width:110px;">Last</th>
                    <th class="text-left" style="width:110px;">First</th>
                    <th class="text-left" style="width:170px;">Guardian</th>
                    <th class="text-left" style="width:110px;">Phone</th>
                    <th class="text-left" style="width:110px;">Mobile</th>
                    <th class="text-center" style="width:120px;">Last Visit</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($patients)){ ?>
                    <?php foreach($patients as $val){ ?>
                       <tr>
                            <td class="text-center"><?php echo $val->cabinet_code;?></td>
                            <td class="text-center"><?php echo $val->file_no;?></td>
                            <td><?php echo $val->last_name;?></td>
                            <td><?php echo $val->first_name;?></td>
                            <td><?php echo $val->guardian;?></td>
                            <td><?php echo $val->phone_no;?></td>
                            <td><?php echo $val->mobile_no;?></td>
                            <td><?php echo (!empty($val->last_visit)) ? date("F m, Y", strtotime($val->last_visit)):"";?></td>
                            <td class="text-center">
                            	<div class="dropdown">
								    <a class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks"></span></a>
								    <ul class="dropdown-menu dropdown-menu-left">
								      <li><a href="/patient/edit/<?php echo $val->patient_id;?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Patient Details</a></li>
								      <li><a href="/consultation/add/<?php echo $val->patient_id;?>"><i class="fa fa-file-text-o" aria-hidden="true"></i> Add Consultations</a></li>
								      <li><a href="/patient/record/<?php echo $val->patient_id;?>"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;View Record</a></li>
                                      <li><a href="/patient/add_certificate/<?php echo $val->patient_id;?>"><span class="glyphicon glyphicon-duplicate"></span> Add Medical Certificate</a></li>
								      <?php if($user->user_type_id == 1) {?>
								      		<li><a href="/patient/delete/<?php echo $val->patient_id;?>"><i class="fa fa-times" aria-hidden="true"></i> Delete Patient</a></li>
								      <?php } ?>
								    </ul>
								</div>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>              
        </table>

        <!-- Show pagination links -->
        <div id="pagination">
            <ul class="tsc_pagination">
                <?php foreach ($links as $link) {
                   echo "<li>". $link."</li>";
                } ?>
            </ul>
        </div>

    </div>
</section>

<div id='deleteModal' class="modal modal-danger fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><?php echo gettext("Confirm Deletion");?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo gettext("Are you sure you want to delete?");?></p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="toDeleteId" value="" />
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><?php echo gettext("Close");?></button>
                <button id="confirm_delete" type="button" class="btn btn-danger btn-ok"><?php echo gettext("Confirm Deletion");?></button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#deleteModal').on('show.bs.modal', function(e) {
            $("#confirm_delete").click(function(){
                var href = "/patient/delete/"+ $("#toDeleteId").val();
                $.post(href).done(function(response) {
                   window.location.href = '/patient'
                });
            });
        });
      
    });

    function bz_message_fade(type, target, message, duration) {
        $(target).addClass(type);
        $(target).html(message);
        $(target).fadeIn('slow');
        setTimeout(function () {
            $(target).fadeOut('slow');
        }, duration);
    }

    function addId(id){
        $("#toUpdateId").val(id);
    }

    function addDeleteId(id){
        $("#toDeleteId").val(id);
    }
</script>

<?php
    $content = ob_get_clean();
    $template = $this->load->view('inc/main_template.php', [
        'title'       => "Patients",
        'pagetitle'   => "Patients",
        'breadcrumbs' => [
            [
                'link'  => '/patient/lists',
                'title' => "Patients"
            ]
        ],
        'section' => "Patient",
        'content' => $content
    ]);
?>