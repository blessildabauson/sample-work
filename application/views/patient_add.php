<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php ob_start();?>

<form id="addForm" method="POST" action="/patient/add">
	<section class="panel">
		<header class="panel-heading">
	        <div class="panel-actions">
	            <a href="#" class="fa fa-caret-down"></a>
	            <a href="#" class="fa fa-times"></a>
	        </div>
	        <p class="panel-title">
	        	<h4>Create New Patient</h4>
	        </p>
	    </header>
	    <div class="panel-body">
	    	<fieldset>
	    		<div class="form-group">
            		<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Clinic</label>  
                        <div class="col-md-8">
                            <select name="clinic_id" class="form-control">
		                    	<option value="0">Select from list</option>
		                    	<?php foreach($clinics as $ckey => $cval) { ?>
		                    		<option value="<?php echo $cval->clinic_id?>">
		                    			<?php echo $cval->cname?>
		                    		</option>
		                    	<?php } ?>
		                    </select>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">File No.</label>  
                        <div class="col-md-8">
                            <input name="file_no" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                    	&nbsp;&nbsp;
                    	<label><input type="checkbox" value="1" name="hmo"> HMO </label>&nbsp;&nbsp;
	                	<label><input type="checkbox" value="1" name="philhealth"> Philhealth</label>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Cabinet Code</label>  
                        <div class="col-md-8">
                            <input name="cabinet_code" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Patient Type</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="type">
							  	<option value="1">20 years and below</option>
							  	<option value="2">Adult</option>
							</select>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<label class="col-md-4 control-label" for="textinput">Archived</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="archive">
							  <option value="0">No</option>
							  <option value="1">Yes</option>
							</select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Surname</label>  
                        <div class="col-md-8">
                            <input name="last_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">First Name</label>  
                        <div class="col-md-8">
                            <input name="first_name" type="text" class="form-control">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Middle Name</label>  
                        <div class="col-md-8">
                            <input name="middle_name" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Gender</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="gender">
							  	<option value="male">Male</option>
							  	<option value="female">Female</option>
							</select>
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Birthday</label>  
                        <div class="col-md-8">
                            <div id="bdaypicker" class="input-group date" data-provide="datepicker">
							    <input type="text" class="form-control" name="dob">
							    <div class="input-group-addon">
							        <span class="fa fa-calendar"></span>
							    </div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Guardian</label>  
                        <div class="col-md-8">
                            <input name="guardian" type="text" class="form-control">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Relationship</label>  
                        <div class="col-md-8">
                            <input name="guardian_relationship" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Type of Delivery</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="tod">
							  	<option value="Normal">Normal</option>
							  	<option value="CS">CS</option>
							</select>
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Feeding Type</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="feeding_type">
							  <option value="Breastfeeding">Breastfeeding</option>
							  <option value="Formula">Formula</option>
							  <option value="Mixed">Mixed</option>
							  <option value="Others">Others</option>
							</select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Allergy Risk</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" name="allery_risk"></textarea>
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Medical History</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" name="medical_history"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Phone No</label>  
                        <div class="col-md-8">
                            <input name="phone_no" type="text" class="form-control">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Mobile No</label>  
                        <div class="col-md-8">
                            <input name="mobile_no" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Home Address</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="3" name="address1" ng-model="address1"></textarea>
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Email</label>  
                        <div class="col-md-8">
                            <input name="email" type="text" class="form-control">
                        </div>
                    </div>
                </div>

                <hr/>
			    <a href="javascript:void(0)" data-toggle="collapse" data-target="#info-panel">More Information <span class="glyphicon glyphicon-menu-down"></span></a><br/>
		        <div id="info-panel" class="collapse info-panel" style="margin-top:20px;">
		        	<div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Address 2</label>  
	                        <div class="col-md-8">
	                            <textarea class="form-control" rows="3" name="address2"></textarea>
	                        </div>
	                    </div>
	                	<div class="col-md-6"></div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Father's Name</label>  
	                        <div class="col-md-8">
	                            <input name="father_name" type="text" class="form-control">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Mother's Name</label>  
	                        <div class="col-md-8">
	                            <input name="mother_name" type="text" class="form-control">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Occupation</label>  
	                        <div class="col-md-8">
	                            <input name="father_occupation" type="text" class="form-control">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Occupation</label>  
	                        <div class="col-md-8">
	                            <input name="mother_occupation" type="text" class="form-control">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Contact No</label>  
	                        <div class="col-md-8">
	                            <input name="father_contact" type="text" class="form-control">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Contact No</label>  
	                        <div class="col-md-8">
	                            <input name="mother_contact" type="text" class="form-control">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Father's HMO</label>  
	                        <div class="col-md-8">
	                            <input name="father_hmo" type="text" class="form-control">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Mother's HMO</label>  
	                        <div class="col-md-8">
	                            <input name="mother_hmo" type="text" class="form-control">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Misc Notes</label>  
	                        <div class="col-md-8">
	                            <textarea class="form-control" rows="3" name="misc_notes"></textarea>
	                        </div>
	                    </div>
	                	<div class="col-md-6"></div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Hospital of Birth</label>  
	                        <div class="col-md-8">
	                            <input name="hospital_birth" type="text" class="form-control">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">OB's Name</label>  
	                        <div class="col-md-8">
	                            <input name="ob_name" type="text" class="form-control">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">NB Screening</label>  
	                        <div class="col-md-8">
	                            <input name="nb_screening" type="text" class="form-control">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Other Sreening</label>  
	                        <div class="col-md-8">
	                            <input name="other_screening" type="text" class="form-control">
	                        </div>
	                    </div>
	                </div>
		        </div><br/><br/>
		        <div class="form-group">
		           	<button type="submit" class="btn btn-primary">Submit</button>
		        </div>
	    	</fieldset>
	    </div>
	</section>
</form>

<script>
    $(document).ready(function() {
        $('#bdaypicker').datepicker({
    		format: "yyyy-mm-dd",
		    todayBtn: true,
		    todayHighlight: true
		});
    });
</script>

<?php
    $content = ob_get_clean();
    $template = $this->load->view('inc/main_template.php', [
        'title'       => "Patients - Add",
        'pagetitle'   => "Patients - Add",
        'breadcrumbs' => [
            [
                'link'  => '/patient/lists',
                'title' => "Patients"
            ],
            [
                'link'  => '/patient/add',
                'title' => "Add Patient"
            ]
        ],
        'section' => "Patient",
        'content' => $content
    ]);
?>