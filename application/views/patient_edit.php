<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php ob_start();?>

<form id="addForm" method="POST" action="/patient/edit/<?php echo $patient->patient_id;?>">
    <input type="hidden" name="patient_id" value="<?php echo $patient->patient_id;?>" />
	<section class="panel">
		<header class="panel-heading">
	        <div class="panel-actions">
	            <a href="#" class="fa fa-caret-down"></a>
	            <a href="#" class="fa fa-times"></a>
	        </div>
	        <p class="panel-title">
	        	<h4>Create New Patient</h4>
	        </p>
	    </header>
	    <div class="panel-body">
	    	<fieldset>
	    		<div class="form-group">
            		<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Clinic</label>  
                        <div class="col-md-8">
                            <select name="clinic_id" class="form-control">
		                    	<option value="0">Select from list</option>
		                    	<?php foreach($clinics as $ckey => $cval) { ?>
		                    		<option value="<?php echo $cval->clinic_id?>" <?php echo ($patient->clinic_id == $cval->clinic_id) ? "selected":"";?> >
		                    			<?php echo $cval->cname?>
		                    		</option>
		                    	<?php } ?>
		                    </select>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">File No.</label>  
                        <div class="col-md-8">
                            <input name="file_no" type="text" class="form-control" value="<?php echo $patient->file_no;?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                    	&nbsp;&nbsp;
                    	<label><input type="checkbox" value="1" name="hmo"> HMO </label>&nbsp;&nbsp;
	                	<label><input type="checkbox" value="1" name="philhealth"> Philhealth</label>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Cabinet Code</label>  
                        <div class="col-md-8">
                            <input name="cabinet_code" type="text" class="form-control" value="<?php echo $patient->cabinet_code;?>">
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label" for="textinput">Patient Type</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="type">
                                <option value="" <?php echo ($patient->gender == 0) ? "selected":"";?> >Select from the list</option>
							  	<option value="1" <?php echo ($patient->type == 1) ? "selected":"";?> >20 years and below</option>
							  	<option value="2" <?php echo ($patient->type == 2) ? "selected":"";?> >Adult</option>
							</select>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<label class="col-md-4 control-label" for="textinput">Archived</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="archive">
							  <option value="0" <?php echo ($patient->archive == 0) ? "selected":"";?> >No</option>
							  <option value="1" <?php echo ($patient->archive == 1) ? "selected":"";?> >Yes</option>
							</select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Surname</label>  
                        <div class="col-md-8">
                            <input name="last_name" type="text" class="form-control" value="<?php echo $patient->last_name;?>">
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">First Name</label>  
                        <div class="col-md-8">
                            <input name="first_name" type="text" class="form-control" value="<?php echo $patient->first_name;?>">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Middle Name</label>  
                        <div class="col-md-8">
                            <input name="middle_name" type="text" class="form-control" value="<?php echo $patient->middle_name;?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Gender</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="gender">
                                <option value="" <?php echo ($patient->gender == "") ? "selected":"";?> >Select from the list</option>
							  	<option value="male" <?php echo ($patient->gender == "male") ? "selected":"";?> >Male</option>
							  	<option value="female" <?php echo ($patient->gender == "female") ? "selected":"";?> >Female</option>
							</select>
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Birthday</label>  
                        <div class="col-md-8">
                            <div id="bdaypicker" class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control" name="dob" value="<?php echo $patient->dob;?>">
                                <div class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Guardian</label>  
                        <div class="col-md-8"> 
                            <input name="guardian" type="text" class="form-control" value="<?php echo $patient->guardian;?>">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Relationship</label>  
                        <div class="col-md-8">
                            <input name="guardian_relationship" type="text" class="form-control" value="<?php echo $patient->guardian_relationship;?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Type of Delivery</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="tod">
                                <option value="" <?php echo ($patient->tod == "") ? "selected":"";?> >Select from the list</option>
							  	<option value="Normal" <?php echo ($patient->tod == "Normal") ? "selected":"";?> >Normal</option>
							  	<option value="CS" <?php echo ($patient->tod == "CS") ? "selected":"";?> >CS</option>
							</select>
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Feeding Type</label>  
                        <div class="col-md-8">
                            <select class="form-control" name="feeding_type">
                                <option value="" <?php echo ($patient->feeding_type == "") ? "selected":"";?> >Select from the list</option>
							     <option value="Breastfeeding" <?php echo ($patient->feeding_type == "Breastfeeding") ? "selected":"";?>>Breastfeeding</option>
							     <option value="Formula" <?php echo ($patient->feeding_type == "Formula") ? "selected":"";?>>Formula</option>
							     <option value="Mixed" <?php echo ($patient->feeding_type == "Mixed") ? "selected":"";?>>Mixed</option>
							     <option value="Others" <?php echo ($patient->feeding_type == "Others") ? "selected":"";?>>Others</option>
							</select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">Allergy Risk</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" name="allery_risk"><?php echo $patient->allery_risk;?></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label">Medical History</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" name="medical_history"><?php echo $patient->medical_history;?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Phone No</label>  
                        <div class="col-md-8">
                            <input name="phone_no" type="text" class="form-control" value="<?php echo $patient->phone_no;?>">
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Mobile No</label>  
                        <div class="col-md-8">
                            <input name="mobile_no" type="text" class="form-control" value="<?php echo $patient->mobile_no;?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-6">
                    	<label class="col-md-4 control-label">Home Address</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="3" name="address1" ng-model="address1"><?php echo $patient->address1;?></textarea>
                        </div>
                    </div>
                	<div class="col-md-6">
                        <label class="col-md-4 control-label">Email</label>  
                        <div class="col-md-8">
                            <input name="email" type="text" class="form-control" value="<?php echo $patient->email;?>">
                        </div>
                    </div>
                </div>

                <hr/>
			    <a href="javascript:void(0)" data-toggle="collapse" data-target="#info-panel">More Information <span class="glyphicon glyphicon-menu-down"></span></a><br/>
		        <div id="info-panel" class="collapse info-panel" style="margin-top:20px;">
		        	<div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Address 2</label>  
	                        <div class="col-md-8">
	                            <textarea class="form-control" rows="3" name="address2"><?php echo $patient->address2;?></textarea>
	                        </div>
	                    </div>
	                	<div class="col-md-6"></div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Father's Name</label>  
	                        <div class="col-md-8">
	                            <input name="father_name" type="text" class="form-control" value="<?php echo $patient->father_name;?>">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Mother's Name</label>  
	                        <div class="col-md-8">
	                            <input name="mother_name" type="text" class="form-control" value="<?php echo $patient->mother_name;?>">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Occupation</label>  
	                        <div class="col-md-8">
	                            <input name="father_occupation" type="text" class="form-control" value="<?php echo $patient->father_occupation;?>">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Occupation</label>  
	                        <div class="col-md-8">
	                            <input name="mother_occupation" type="text" class="form-control" value="<?php echo $patient->mother_occupation;?>">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Contact No</label>  
	                        <div class="col-md-8">
	                            <input name="father_contact" type="text" class="form-control" value="<?php echo $patient->father_contact;?>">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Contact No</label>  
	                        <div class="col-md-8">
	                            <input name="mother_contact" type="text" class="form-control" value="<?php echo $patient->mother_contact;?>">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Father's HMO</label>  
	                        <div class="col-md-8">
	                            <input name="father_hmo" type="text" class="form-control" value="<?php echo $patient->father_hmo;?>">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Mother's HMO</label>  
	                        <div class="col-md-8">
	                            <input name="mother_hmo" type="text" class="form-control" value="<?php echo $patient->mother_hmo;?>">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Misc Notes</label>  
	                        <div class="col-md-8">
	                            <textarea class="form-control" rows="3" name="misc_notes"><?php echo $patient->misc_notes;?></textarea>
	                        </div>
	                    </div>
	                	<div class="col-md-6"></div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">Hospital of Birth</label>  
	                        <div class="col-md-8">
	                            <input name="hospital_birth" type="text" class="form-control" value="<?php echo $patient->hospital_birth;?>">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">OB's Name</label>  
	                        <div class="col-md-8">
	                            <input name="ob_name" type="text" class="form-control" value="<?php echo $patient->ob_name;?>">
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-6">
	                    	<label class="col-md-4 control-label">NB Screening</label>  
	                        <div class="col-md-8">
	                            <input name="nb_screening" type="text" class="form-control" value="<?php echo $patient->nb_screening;?>">
	                        </div>
	                    </div>
	                	<div class="col-md-6">
	                        <label class="col-md-4 control-label">Other Sreening</label>  
	                        <div class="col-md-8">
	                            <input name="other_screening" type="text" class="form-control" value="<?php echo $patient->other_screening;?>">
	                        </div>
	                    </div>
	                </div>
		        </div><br/><br/>
		        <div class="form-group">
		           	<button type="submit" class="btn btn-primary">Submit</button>
		        </div>
	    	</fieldset>
	    </div>
	</section>
</form>

<script>
    $(document).ready(function() {
        $('#bdaypicker').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: true,
            todayHighlight: true
        });
    });
</script>

<?php
    $content = ob_get_clean();
    $template = $this->load->view('inc/main_template.php', [
        'title'       => "Patients - Edit",
        'pagetitle'   => "Patients - Edit",
        'breadcrumbs' => [
            [
                'link'  => '/patient/lists',
                'title' => "Patients"
            ],
            [
                'link'  => '/patient/edit',
                'title' => "Edit Patient"
            ]
        ],
        'section' => "Patient",
        'content' => $content
    ]);
?>